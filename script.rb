#!/usr/bin/env ruby
NOOSFERO_ROOT = File.expand_path(File.join(File.dirname(__FILE__), '/../'))
require File.expand_path(File.join(NOOSFERO_ROOT, 'config', 'environment'))
env = Environment.default
tecciencia = Community["tecciencia"]
tec_children = Community.children(tecciencia).to_a
tec_children << tecciencia
communities = env.profiles.select{ |profile| profile.is_a? Community }
communities_with_tecciencia_theme = communities.select{ |community| community.theme == "tecciencia"}
communities_with_tecciencia_theme.each do |community|
  unless tec_children.include? community
    SubOrganizationsPlugin::Relation.add_children(tecciencia, community)
  end
end
